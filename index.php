<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>PesquisaCom.com</title>
	<meta name="description" content="Site de Pesquisa de Comercio de Bairro">
	<meta name="robots" content="index, follow">
	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300i,400,400i,700i" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div id="bar-top">
		<h1 class="icon-tel">Phone icon</h1>
		<p>(21) 3116-6710</p>
		<div class="line-vertical"></div>
		<p>Siga-nos:</p>
		<img src="img/icon-face-top.png">
		<img src="img/icon-insta-top.png">
		<img src="img/icon-twitter-top.png">
	</div><!--Bar Top-->
	<div id="top">
		<a href=""><h1 class="logo-mobile">Pesquisa Com.com</h1></a>
		<div class="line-vertical-top"></div>
		<p>Todos os Comércios que você precisa em um só lugar!</p>
		
	</div><!--Top-->
	<div id="menu">
		<button class="btn-menu"><i class="fa fa-bars fa-lg" aria-hidden="true"></i><p>Menu</p></button>
		<nav class="menu">
			<a class="btn-close">x</a>
			<ul>
				<li><a href="#">Página Inicial</a></li>
				<li><a href="#">Empresa</a></li>
				<li><a href="#">Empresa</a></li>
				<li><a href="#">Contato</a></li>
			</ul>
		</nav>
	</div><!--Menu-->

	<div id="categoria">
		<button class="btn-categoria"><i class="fa fa-folder-open fa-lg" aria-hidden="true"></i><p>Categorias</p></button>
		<nav class="categoria">
			<a class="btn-close-categoria">x</a>
			<ul>
				<li><a href="#"><h1 class="icon-restaurantes">Phone icon</h1>Restaurantes</a></li>
				<li><a href="#"><h1 class="icon-lojas">Phone icon</h1>Lojas de Roupa</a></li>
				<li><a href="#"><h1 class="icon-saude">Phone icon</h1>Saúde</a></li>
				<li><a href="#"><h1 class="icon-imobiliaria">Phone icon</h1>Imobiliária</a></li>
				<li><a href="#"><h1 class="icon-beleza">Phone icon</h1>Beleza</a></li>
				<li><a href="#"><h1 class="icon-mercados">Phone icon</h1>Mercados</a></li>
				<li><a href="#"><h1 class="icon-petshop">Phone icon</h1>Petshop</a></li>
				<li><a href="#"><h1 class="icon-advocacia">Phone icon</h1>Advocacia</a></li>
			</ul>
		</nav>
	</div><!--Menu-->

	<div id="slide">
		<img src="img/slide.png">
	</div><!--Slide-->

	<div id="area-prata">
		<p>Plano</p>
		<h1>Prata</h1>
		<div class="box">
			<img src="img/img-destaque.png">
			<i class="fa fa-home fa-lg icon-box" aria-hidden="true"></i><span>IP Conceito</span><br>
			<i class="fa fa-phone fa-lg icon-box" aria-hidden="true"></i><span>(21) 3144-5566</span><br>
			<i class="fa fa-location-arrow icon-box" aria-hidden="true"></i><span>Av. Engenheiro Souza Filho, nº23</span><br>
		</div><!--Box-->
		<div class="box">
			<img src="img/img-destaque.png">
			<i class="fa fa-home fa-lg icon-box" aria-hidden="true"></i><span>IP Conceito</span><br>
			<i class="fa fa-phone fa-lg icon-box" aria-hidden="true"></i><span>(21) 3144-5566</span><br>
			<i class="fa fa-location-arrow icon-box" aria-hidden="true"></i><span>Av. Engenheiro Souza Filho, nº23</span><br>
		</div><!--Box-->
		<div class="box">
			<img src="img/img-destaque.png">
			<i class="fa fa-home fa-lg icon-box" aria-hidden="true"></i><span>IP Conceito</span><br>
			<i class="fa fa-phone fa-lg icon-box" aria-hidden="true"></i><span>(21) 3144-5566</span><br>
			<i class="fa fa-location-arrow icon-box" aria-hidden="true"></i><span>Av. Engenheiro Souza Filho, nº23</span><br>
		</div><!--Box-->
		<div class="box">
			<img src="img/img-destaque.png">
			<i class="fa fa-home fa-lg icon-box" aria-hidden="true"></i><span>IP Conceito</span><br>
			<i class="fa fa-phone fa-lg icon-box" aria-hidden="true"></i><span>(21) 3144-5566</span><br>
			<i class="fa fa-location-arrow icon-box" aria-hidden="true"></i><span>Av. Engenheiro Souza Filho, nº23</span><br>
		</div><!--Box-->
	</div><!--Área Prata-->

	<div id="area-bronze">
		<p>Plano</p>
		<h1>Bronze</h1>
		<div class="box">
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
		</div><!--Box-->
		<div class="box">
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
		</div><!--Box-->
		<div class="box">
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
		</div><!--Box-->
		<div class="box">
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
			<a href="#">IP Conceito</a>
		</div><!--Box-->
	</div><!--Área Bronze-->

	<div id="newsletter">
		<p>Receba semanalmente e-mails com promoções e novos comércios cadastrados em nossa plataforma.</p>
		<form action="http://www.laleli.com.br/" method="post">
			<input id="form" type="text" name="s" id="s" placeholder="Insira aqui o seu e-mail" value="">
			<input id="btn" type="submit" name="" value="Assinar">
		</form>		
	</div><!-- Newsletter -->

	<div id="footer">
		<div class="box">
			<h1>Entre em Contato</h1>
			<p><img class="icon-footer" src="img/icon-tel-footer.png">(21) 2233-4455</p>
			<p><img class="icon-footer" src="img/icon-email-footer.png">contato@pesquisacom.com</p>
			<p><img class="icon-footer" src="img/icon-wpp-footer.png">(21) 92233-4455</p>
			<p><img class="icon-footer" src="img/icon-hora-footer.png">De seg à sex das 09:00h às 19:00h</p>
		</div><!--Box-->
		<div class="box">
			<h1>Curta Nossa Página</h1>
			<div class="fb-page" data-href="https://www.facebook.com/pesquisacom/?fref=ts" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pesquisacom/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pesquisacom/?fref=ts">Pesquisa Com.com</a></blockquote></div>
		</div><!--Box-->
		<div class="box">
			<h1>Siga-nos nas Redes Sociais</h1>
			<p>Fique por dentro das novidades, interaja com a comunidade, receba dicas interessantes diariamente, ou simplesmente mostre ao mundo que você faz parte desse projeto sensacional!</p>
			<img src="img/icon-footer-face.png">
			<img src="img/icon-footer-insta.png">
			<img src="img/icon-footer-twitter.png">
		</div><!--Box-->
	</div><!--Footer-->

	<div id="copyright">
		<p>Copyright 2017 - Plataforma de Anúncio de Bairo Online - Todos os Direitos Reservados</p>
	</div><!--Copyright-->
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=137592999765927";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script>
	$(".btn-menu").click(function(){
		$(".menu").show();
	});
	$(".btn-close").click(function(){
		$(".menu").hide();
	});
	</script>
	<script>
	$(".btn-categoria").click(function(){
		$(".categoria").show();
	});
	$(".btn-close-categoria").click(function(){
		$(".categoria").hide();
	});
	</script>
</body>
</html>